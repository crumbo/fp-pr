pipeline {

    agent {
        node {
            label 'node'
        }
    }

    stages {
        stage('pull') {
            steps {
                withEnv(['PATH+EXTRA=/usr/sbin:/usr/bin:/sbin:/bin']) {
                    sh """
                    cd ~/git/fp-pr && git pull
                    """
                }
            }
        }

        stage('build') {
            steps {
                withEnv(['PATH+EXTRA=/usr/sbin:/usr/bin:/sbin:/bin']) {
                    sh """
                    cd ~/git/fp-pr && docker compose up -d --build
                    """
                }
            }
        }
    }

    post {
        always {
            cleanWs()
        }

        success {
            withCredentials([string(credentialsId: 'tg-bot-token', variable: 'TOKEN'), string(credentialsId: 'tg-chat-id', variable: 'CHAT_ID')]) {
                sh """
                    curl -s -X POST https://api.telegram.org/bot${TOKEN}/sendMessage -d chat_id=${CHAT_ID} -d parse_mode=markdown -d text='*${env.JOB_NAME}* - *Branch*: ${env.GIT_BRANCH} *Build* : OK'
                """
            }
        }

        aborted {
            withCredentials([string(credentialsId: 'tg-bot-token', variable: 'TOKEN'), string(credentialsId: 'tg-chat-id', variable: 'CHAT_ID')]) {
                sh  ("""
                    curl -s -X POST https://api.telegram.org/bot${TOKEN}/sendMessage -d chat_id=${CHAT_ID} -d parse_mode=markdown -d text='*${env.JOB_NAME}* - *Branch*: ${env.GIT_BRANCH} *Build* : Aborted'
                """)
        }

        }
        failure {
            withCredentials([string(credentialsId: 'tg-bot-token', variable: 'TOKEN'), string(credentialsId: 'tg-chat-id', variable: 'CHAT_ID')]) {
                sh  ("""
                    curl -s -X POST https://api.telegram.org/bot${TOKEN}/sendMessage -d chat_id=${CHAT_ID} -d parse_mode=markdown -d text='*${env.JOB_NAME}* - *Branch*: ${env.GIT_BRANCH} *Build* : Failure'
                """)
            }
        }
    }
}
