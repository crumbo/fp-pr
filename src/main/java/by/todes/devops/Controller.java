package by.todes.devops;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sun.misc.Signal;

import java.math.BigInteger;

@Slf4j
@RestController
@RequestMapping("/")
@RequiredArgsConstructor
public class Controller {

    private final LogRepository repository;

    @GetMapping(path = "/echo/{str}")
    public ResponseEntity echo1(@PathVariable("str") String str) {
        LogEntity logEntity = new LogEntity();
        logEntity.setStr(str);
        repository.saveAndFlush(logEntity);
        str += "\n";
        log.info(str);
        return ResponseEntity.ok(str);
    }

}
